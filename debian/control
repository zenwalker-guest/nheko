Source: nheko
Maintainer: Matrix Packaging Team <pkg-matrix-maintainers@alioth-lists.debian.net>
Uploaders: Hubert Chathi <uhoreg@debian.org>
Section: net
Priority: optional
Build-Depends: asciidoc-base,
               cmake (>= 3.15),
               debhelper-compat (=13),
               dh-exec,
               libcmark-dev (>= 0.29),
               libcurl4-openssl-dev,
               libevent-dev,
               libgstreamer1.0-dev (>= 1.18.0),
               libgstreamer-plugins-base1.0-dev (>= 1.18.0),
               libgstreamer-plugins-bad1.0-dev (>= 1.18.0),
               liblmdb-dev,
               libnice-dev,
               libolm-dev (>= 3.2.7~),
               libqt5svg5-dev,
               libspdlog-dev,
               libsodium-dev,
               libssl-dev,
               libxcb-ewmh-dev,
               nlohmann-json3-dev (>= 3.7.0-2~),
               qt5keychain-dev,
               qtbase5-dev,
               qtdeclarative5-dev,
               libfmt-dev,
               qtquickcontrols2-5-dev,
               qtmultimedia5-dev,
               qttools5-dev,
               zlib1g-dev
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/matrix-team/nheko
Vcs-Git: https://salsa.debian.org/matrix-team/nheko.git
Homepage: https://github.com/Nheko-Reborn/nheko
Rules-Requires-Root: no

Package: nheko
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libqt5multimedia5-plugins,
         qml-module-qt-labs-animation,
         qml-module-qt-labs-platform,
         qml-module-qt-labs-settings,
         qml-module-qtgraphicaleffects,
         qml-module-qtmultimedia,
         qml-module-qtquick2,
         qml-module-qtquick-controls2,
         qml-module-qtquick-layouts,
         qml-module-qtquick-window2,
         gstreamer1.0-nice,
         gstreamer1.0-qt5,
         gstreamer1.0-vaapi
Recommends: ca-certificates,
            fonts-noto-color-emoji,
            kimageformat-plugins,
            qt5-image-formats-plugins
Description: desktop IM client for the Matrix protocol
 Nheko is a Qt-based chat client for Matrix, an open, federated communications
 protocol.  The motivation behind the project is to provide a native desktop
 app for Matrix that feels more like a mainstream chat app and less like an IRC
 client.
